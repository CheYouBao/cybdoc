#cyb os项目相关文档

##项目文档使用git管理
* 请使用以下命令来随时更新最新的文档
```
#!shell
cd [your_doc_path]
git clone -o bitbucket https://bitbucket.org/CheYouBao/cybdoc
```
* 请使用以下命令来查看文档的更改历史
```
#!shell
cd [your_doc_path]
git log -p
```
* 如需提交内容，请注册bitbucket账户之后使用以下命令来提交文档的更改
```
#!shell
cd [your_doc_path]
git add --all
git commit -m "这里写上本次更改的相关说明"
git push bitbucket master
```

#cyb服务端接口测试工具
##使用方法
参见cybapitest目录下的README.md

# CheYouOS Build Guide #

## **编译环境** ##
* 系统：
Ubuntu 14.04(64bit) [http://www.ubuntu.com/download](http://www.ubuntu.com/download)
* 依赖包：
```bash
$ sudo apt-get install git git-core gnupg flex bison gperf build-essential zip curl zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z-dev ccache libgl1-mesa-dev libxml2-utils xsltproc unzip libesd0-dev liblz4-tool libncurses5-dev libsdl1.2-dev libwxgtk2.8-dev libxml2 lzop maven openjdk-7-jdk openjdk-7-jre pngcrush schedtool squashfs-tools lib32readline-gplv2-dev lib32z1-dev
```

## **解压源码** ##
```bash
$ mkdir -p ~/CheYouBao
$ tar -xzvf /path/of/the/source/CheYouOS.tar.gz -C ~/CheYouBao
```

## **编译完整包** ##
```bash
### 编译aries(MI 2S) ###
$ cd ~/CheYouBao
$ source build/envsetup.sh
$ lunch cm_aries-user
$ mka otapackage 或 make -j8 otapackage
### 主机内存不足或者其他原因(非源码本身原因)导致编译中断时，继续执行mka otapackage 或 make -j8 otapackage即可继续编译 ###
```

```bash
### 编译hammerhead(Nexus 5) ###
$ cd ~/CheYouBao
$ source build/envsetup.sh
$ lunch cm_hammerhead-user
$ mka otapackage 或 make -j8 otapackage
### 主机内存不足或者其他原因(非源码本身原因)导致编译中断时，继续执行mka otapackage 或 make -j8 otapackage即可继续编译 ###
```

```bash
### 编译nx403a(Z5S mini) ###
$ cd ~/CheYouBao
$ source build/envsetup.sh
$ lunch cm_nx403a-user
$ mka otapackage 或 make -j8 otapackage
### 主机内存不足或者其他原因(非源码本身原因)导致编译中断时，继续执行mka otapackage 或 make -j8 otapackage即可继续编译 ###
```

```bash
### 编译bacon(OnePlus One) ###
$ cd ~/CheYouBao
$ source build/envsetup.sh
$ lunch cm_bacon-user
$ mka otapackage 或 make -j8 otapackage
### 主机内存不足或者其他原因(非源码本身原因)导致编译中断时，继续执行mka otapackage 或 make -j8 otapackage即可继续编译 ###
```

## **编译产物** ##
### 说明：单次编译出来的差分包和完整包的$ID是一一对应的
### 警告：重新编译完整包会删除上一次编译的差分包和完整包，注意备份差分包和完整包到其他目录(非~/CheYouBao/out目录)
### 警告：重新编译完整包会删除上一次编译的差分包和完整包，注意备份差分包和完整包到其他目录(非~/CheYouBao/out目录)
### 警告：重新编译完整包会删除上一次编译的差分包和完整包，注意备份差分包和完整包到其他目录(非~/CheYouBao/out目录)
```bash
### 差分包(用于制作OTA) ###
~/CheYouBao/out/target/product/aries/obj/PACKAGING/target_files_intermediates/cm_aries-target_files-$ID1.zip
~/CheYouBao/out/target/product/hammerhead/obj/PACKAGING/target_files_intermediates/cm_hammerhead-target_files-$ID2.zip
~/CheYouBao/out/target/product/nx403a/obj/PACKAGING/target_files_intermediates/cm_nx403a-target_files-$ID3.zip
~/CheYouBao/out/target/product/bacon/obj/PACKAGING/target_files_intermediates/cm_bacon-target_files-$ID4.zip
```

```bash
### 完整包 ###
~/CheYouBao/out/target/product/aries/cm_aries-ota-$ID1.zip
~/CheYouBao/out/target/product/hammerhead/cm_hammerhead-ota-$ID2.zip
~/CheYouBao/out/target/product/nx403a/cm_nx403a-ota-$ID3.zip
~/CheYouBao/out/target/product/bacon/cm_bacon-ota-$ID4.zip
```

## **重新编译完整包** ##
```bash
### 重新编译aries(MI 2S) ###
$ cd ~/CheYouBao
$ source build/envsetup.sh
$ lunch cm_aries-user
### 清空aries(MI 2S)的编译产物 ###
$ make installclean
### 开始编译 ###
$ mka otapackage 或 make -j8 otapackage
### 主机内存不足或者其他原因(非源码本身原因)导致编译中断时，继续执行mka otapackage 或 make -j8 otapackage即可继续编译 ###
```

```bash
### 重新编译hammerhead(Nexus 5) ###
$ cd ~/CheYouBao
$ source build/envsetup.sh
$ lunch cm_hammerhead-user
### 清空hammerhead(Nexus 5)的编译产物 ###
$ make installclean
### 开始编译 ###
$ mka otapackage 或 make -j8 otapackage
### 主机内存不足或者其他原因(非源码本身原因)导致编译中断时，继续执行mka otapackage 或 make -j8 otapackage即可继续编译 ###
```

```bash
### 重新编译nx403a(Z5S mini) ###
$ cd ~/CheYouBao
$ source build/envsetup.sh
$ lunch cm_nx403a-user
### 清空nx403a(Z5S mini) 的编译产物 ###
$ make installclean
### 开始编译 ###
$ mka otapackage 或 make -j8 otapackage
### 主机内存不足或者其他原因(非源码本身原因)导致编译中断时，继续执行mka otapackage 或 make -j8 otapackage即可继续编译 ###
```

```bash
### 重新编译bacon(OnePlus One) ###
$ cd ~/CheYouBao
$ source build/envsetup.sh
$ lunch cm_bacon-user
### 清空nx403a(Z5S mini) 的编译产物 ###
$ make installclean
### 开始编译 ###
$ mka otapackage 或 make -j8 otapackage
### 主机内存不足或者其他原因(非源码本身原因)导致编译中断时，继续执行mka otapackage 或 make -j8 otapackage即可继续编译 ###
```

## **清空编译产物** ##
### 说明：当编译出来的ROM出现以前没有出现过的奇怪现象、BUG或源码有非常大改动时执行，等同于清空~/CheYouBao/out目录，清空完后需要重新**编译完整包**
```bash
$ source build/envsetup.sh
$ make clean
```

## **制作OTA zip包** ##
### 说明：如果完整包作为OTA zip包，忽略此步
```bash
### 必须先进入源码根目录 ###
$ cd ~/CheYouBao
### 举例：上一版本差分包.zip和最新版本差分包.zip在**编译产物**里有说明 ###
$ ./build/tools/releasetools/ota_from_target_files -v -n -i /the/path/of/上一版本差分包.zip /the/path/of/最新版本差分包.zip ~/ota增量包.zip
### 更具体的例子 ###
$ ./build/tools/releasetools/ota_from_target_files -v -n -i ~/cm_nx403a-target_files-e0ce3f7a9c.zip ~/cm_nx403a-target_files-d22ddd43ed.zip ~/ota-2016041401-to-2016042801.zip
```

## **处理OTA zip包** ##
### 说明：仅用于处理OTA包以保证OTA包能正常使用，执行前至少需要编译过某个机器的完整包
```bash
$ chmod a+x re_zip_ota
### 举例：~/CheYouBao是车友源码根目录，ota_verify每次新的ota都要修改 ###
$ ./re_zip_ota ~/CheYouBao /the/path/of/ota增量包或完整包.zip /the/path/of/ota_verify
### 更具体的例子 ###
### 处理完整包时 ###
$ ./re_zip_ota ~/CheYouBao ~/cm_nx403a-ota-88ac9b5635.zip ~/ota_verify
### 处理OTA zip包时 ###
$ ./re_zip_ota ~/CheYouBao ~/ota-2016041401-to-2016042801.zip ~/ota_verify
### 执行结果 ###
done! final ota at ~/otawork/ota-final.zip
### ~/otawork/ota-final.zip手动重命名发布即可 ###
```

## **关于ota_verify**
说明：ota包内需要放置一个纯文本文件命名为ota_verify，所有通过在线升级的zip包都需要此文件来校验版本的正确性。
其为一个json文本，格式如下：

```json
{
	"version_code":[
		"2016040801"
	]
}
```

version_code为一个字符串数组，存在数组中的版本号表示可以使用此ota包的版本号，对应于/system/build.prop里ro.cyb.version_code的值。

```json
{
	"version_code":[
		"all"
	]
}
```
如果为all时，表示所有的版本都可以使用此ota包。这种情况一般为使用完整包来ota时出现的。

如果使用手动升级的话，不会检测ota_verify的具体内容，但会判断ota_verify文件是否存在，如果存在，则刷入之前不会wipe data，如果不存在，则刷入之前会wipe data。

## **更新预编译文件** ##
```
#!shell
### 以下路径都是相对于源码根目录~/CheYouBao ###
* 系统版本信息(对应系统文件/system/build.prop)：
./build/core/main.mk
407行到428行如下：
# 通知白名单功能是否开启，为true时开启（只有白名单里的应用才能有通知），为false时关闭（所有应用都能显示通知）
# 白名单源码位置vendor/cm/prebuilt/common/etc/cyb_notify_package_whitelist.conf，手机系统位置/system/etc/cyb_notify_package_whitelist.conf
ADDITIONAL_BUILD_PROPERTIES += ro.cyb.notify.wl.enabled=true

# 安装应用白名单功能是否开启，为true时开启（只有白名单里的应用才能安装），为false时关闭（所有应用都能安装）
# 白名单源码位置vendor/cm/prebuilt/common/etc/cyb_allow_install_package_whitelist.conf，手机系统位置/system/etc/cyb_allow_install_package_whitelist.conf
ADDITIONAL_BUILD_PROPERTIES += ro.cyb.install.wl.enabled=true

# 每次更新必改！！
ADDITIONAL_BUILD_PROPERTIES += ro.cyb.version_code=2016050602

# 可改可不改
ADDITIONAL_BUILD_PROPERTIES += ro.cyb.version_name=v1.1

# 无更新时不用改
ADDITIONAL_BUILD_PROPERTIES += ro.cyb.debug.urlprefix=http://testv.cheyou18.com:8088/cyb

# 无更新时不用改
ADDITIONAL_BUILD_PROPERTIES += ro.cyb.release.urlprefix=http://w.cheyou18.com:80/cyb

# 发行版都不改，内部测试可以改成ro.cyb.mode=debug
ADDITIONAL_BUILD_PROPERTIES += ro.cyb.mode=release

* 壁纸路径：
./frameworks/base/core/res/res/drawable-sw600dp-nodpi/default_wallpaper.jpg
./frameworks/base/core/res/res/drawable-sw720dp-nodpi/default_wallpaper.jpg
./frameworks/base/core/res/res/drawable-nodpi/default_wallpaper.jpg
./vendor/cm/overlay/common/frameworks/base/core/res/res/drawable-hdpi/default_wallpaper.jpg
./vendor/cm/overlay/common/frameworks/base/core/res/res/drawable-sw600dp-nodpi/default_wallpaper.jpg
./vendor/cm/overlay/common/frameworks/base/core/res/res/drawable-sw720dp-nodpi/default_wallpaper.jpg
./vendor/cm/overlay/common/frameworks/base/core/res/res/drawable-nodpi/default_wallpaper.jpg
./vendor/cm/overlay/common/frameworks/base/core/res/res/drawable-xhdpi/default_wallpaper.jpg

* 预编译APK的apk路径：
./vendor/cm/prebuilt/common/app/
./vendor/cm/prebuilt/common/priv-app/

* 预编译APK的so路径(优先选择armeabi-v7a下的so，没有就选armeabi下的so)：
./vendor/cm/prebuilt/common/lib/

* 预编译APK加到源码里编译参考./vendor/cm/config/common.mk里的144行至157行如下：
# Add EasyBaoCYOS
PRODUCT_COPY_FILES += \
    vendor/cm/prebuilt/common/app/EasyBaoCYOS.apk:system/app/EasyBaoCYOS.apk \
    vendor/cm/prebuilt/common/lib/libavcodec-56.so:system/lib/libavcodec-56.so \
    vendor/cm/prebuilt/common/lib/libavdevice-56.so:system/lib/libavdevice-56.so \
    vendor/cm/prebuilt/common/lib/libavfilter-5.so:system/lib/libavfilter-5.so \
    vendor/cm/prebuilt/common/lib/libavformat-56.so:system/lib/libavformat-56.so \
    vendor/cm/prebuilt/common/lib/libavutil-54.so:system/lib/libavutil-54.so \
    vendor/cm/prebuilt/common/lib/libpostproc-53.so:system/lib/libpostproc-53.so \
    vendor/cm/prebuilt/common/lib/libsfftranscoder.so:system/lib/libsfftranscoder.so \
    vendor/cm/prebuilt/common/lib/libswresample-1.so:system/lib/libswresample-1.so \
    vendor/cm/prebuilt/common/lib/libswscale-3.so:system/lib/libswscale-3.so \
    vendor/cm/prebuilt/common/lib/libbspatch.so:system/lib/libbspatch.so \
    vendor/cm/prebuilt/common/lib/liblocSDK6a.so:system/lib/liblocSDK6a.so

* 开机动画路径：
./vendor/cm/prebuilt/common/bootanimation/目录及其子目录下的所有zip包(包含不同分辨率)

* 系统允许安装的APP白名单路径(一行一个包名，不留空白行)：
./vendor/cm/prebuilt/common/etc/cyb_allow_install_package_whitelist.conf

* 通知栏通知 APP白名单路径(一行一个包名，不留空白行)：
./vendor/cm/prebuilt/common/etc/cyb_notify_package_whitelist.conf
```

## **单独编译某个模块** ##
### 说明：用于快速调试
```bash
### 以编译CybLauncher为例，假如正在用N5测试 ###
$ cd ~/CheYouBao
$ source build/envsetup.sh
$ lunch cm_hammerhead-user
$ mmm -j8 frameworks/base/packages/CybLauncher 或者 cd frameworks/base/packages/CybLauncher && mm -j8
### 或者强制重新编译 ###
$ mmm -B -j8 frameworks/base/packages/CybLauncher 或者 cd frameworks/base/packages/CybLauncher && mm -B -j8
### 编译完后终端会提示CybLauncher.apk编译到哪个路径了，adb push 到系统里 ###
```