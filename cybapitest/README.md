#车友宝restful服务器api测试

##系统环境要求

linux环境或mac osx环境，java 1.7


需要下载jq(用于json解析和格式化输出)

linux下直接通过apt来下载安装

```
$ sudo apt-get install jq
```

mac osx需要首先安装homebrew，然后使用brew安装jq

安装brew(安装过程可能需要翻墙，已经安装过则跳过这一步)

```
$ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

使用brew安装jq

```
brew install jq
```



##目录结构

```
.
├── README.md              #说明
├── api                    #api目录，对应每个api请求
│   ├── ad
│   ├── get_member_id
│   ├── get_token
│   ├── ota
│   ├── qrcode
│   ├── score
│   └── weather
├── common.config          #公共参数配置
├── debug.config           #测试环境(debug)参数配置
├── release.config         #生产环境(release)参数配置
├── env.config             #环境选择器
├── gsign.jar              #用于计算sign的jar
├── memberid               #memberid存储文件
└── token                  #token存储文件

```


##配置修改

首先需要手动修改一下common.config

```
#设置java环境，如果已经配置好了，则注释下面的两行代码
export JAVA_HOME="/Library/Java/JavaVirtualMachines/jdk1.7.0_79.jdk/Contents/Home"
export CLASS_PATH=".:$JAVA_HOME/lib"
```

修改这里的java环境变量，如果已经配置了全局的java环境，则注释这两行

```
export equipmentid=00001359125050529470                #使用此设备号来
export device=hammerhead                               #使用此设备型号
export version_code=2016050602                         #使用此版本号
```
修改这里的一些和设备相关的信息

##关于环境选择器

env.config用于选择使用debug.config或者release.config，可以修改此文件来使用不同的配置。

当然也可以新增一些配置。

这里测试环境和生产环境的主要不同在于url的前缀不同，url的后面一部分都是一致的，因此可以简单的通过配置选择器来调用不同的测试环境

##使用

需要使工作目录位于cybapitest目录下

```bash
$ cd cybapitest
```

然后就可以调用api目录下的脚本来进行api测试了

例如测试天气的api

```bash
$ ./api/weather
```


##api扩展

整个工具使用shell来实现，可以依照已有的api来扩展其它的api

工作原理很简单，参照cybapitest/api/目录下的脚本即可