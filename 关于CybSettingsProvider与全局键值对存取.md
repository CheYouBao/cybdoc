#关于CybSettingsProvider与全局键值对存取

##在系统中有一些内容或设置项目需要全局可存取，为了使系统和车友保等app都能方便的使用这个数据，cyb os提高CybSettingsProvider服务来支持这一功能

##需要先在app中的java build path引入CybSettingsLib.jar包

##关于如何存取，可通过下面的示例代码来了解
```
#!java

//保存boolean数据，key为key1
CybSettings.saveBoolean(mContext, "key1", true);

//保存int数据，key为key2
CybSettings.saveInt(mContext, "key2", 32);

//保存String数据，key为key3
CybSettings.saveString(mContext, "key3", "hello");

//获取boolean数据，key为key1，默认值为true
CybSettings.getBoolean(mContext, "key1", true);

//获取int数据，key为key2，默认值为12
CybSettings.getInt(mContext, "key2", 12);

//获取String数据，key为key3，默认值为fine
CybSettings.getString(mContext, "key3", "fine");

```

###memberID数据
* key == member_id
* type == String
* default == ""

###是否开启tts的全局设置项
* key == cyb_tts_switch
* type == boolean
* default == true


