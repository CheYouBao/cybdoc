
## 手机型号与开发者代号device一览表

### 说明
从左往右依次为：厂商，机型，开发者代号



| Manufacturer      | Model              |       Device        |
| :---------------: | :----------------: | :-----------------: |
| LGE               | Nexus 5            | hammerhead          |
| Xiaomi            | Mi2                | aries               |
| Nubia             | Z5S mini           | nx403a              |