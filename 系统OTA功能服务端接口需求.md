#系统OTA功能服务端接口需求

###提供ota检查更新的url

如http://update.fiui.php/ota.php，

访问方式：get

参数：

device -- String型，设备代号

version_code -- String型，设备上当前正在运行的系统版本code

可以访问下面的示例接口来查看示例返回json数据

[http://update.fiui.org/ota.php?device=bacon&version_code=2015_12_20](http://update.fiui.org/ota.php?device=bacon&version_code=2015_12_20)



###接口返回数据按照示例接口的json数据形式提供

key说明：

device -- String型，设备代号

version_code -- String型，由get提交的version_code升级到下一版本的版本code

version_name -- String型，由get提交的version_code升级到下一版本的版本名

date -- long型，标准unix时间戳，表示该更新包的上线时间

size -- long型，更新包大小，单位byte

md5 -- String型，更新包md5校验码

auto_update -- boolean型，是否不需要用户干涉，自动下载并安装此更新包

url -- String型，ota包的直接下载链接
