#系统与车友保app之间的广播约定

### 行车记录仪是否开启的状态变更广播
* sender:车友保app
* reciever:系统
* action
```
#!shell
cyb_video_record_status
```
* extras:
```
#!shell
status  #boolean型，状态，true为正在录制，false为没有录制
```

### 更新桌面图标右上角通知数字的广播
* sender:all app
* reciever:系统
* action
```
#!shell
cyb_app_noti_count
```
* extras:
```
#!shell
package  #String型，为应用的包名
count  #int型，为要显示的通知数
```

### 断电自动关机广播
* sender:系统
* reciever:车友保app
* action
```
#!shell
cyb_os_auto_shutdown
```
* extras:无


### 电源键按下事件广播
* sender:系统
* reciever:车友保app
* action
```
#!shell
cyb_os_power_key_down
```
* extras:无