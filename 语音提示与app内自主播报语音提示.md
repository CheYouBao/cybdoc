#app自主播报语音提示

###语音播报全局开关
* 见CybSettingsProvider中全局键值对存取相关的内容

###如果在app中需要自主播报语音，请使用文档目录下的VoiceTtsLib.jar包
```
#!java
//在java build path中引入VoiceTtsLib.jar
//使用下面的示例代码立即播报语音文字
//第三个参数对应语音的类型，即设置里的两个语音的开关

//MODE_PROMPT
//SD状态、行车记录开启成功/失败、网络接入成功/失败，GPS连接正常/失败

//MODE_NOTIFICATION
//天气预报、整点报时，通知信息

VoiceTts.speak(mContext, "你好", VoiceTts.MODE_PROMPT);
```

